
_Group = function(game) { //x, y) {

    // Call parent constructor
    Phaser.Group.call(this, game); 

    // Store copy
    this.game = game;
    this.frameCounter = 0; // Keep count of actual framerate
    this.updateCounter = 0; // Keep count of visible framerate

    // _Group
    this.duck = new Phaser.Sprite(this.game, 0, 0, 'duck_quest');
    this.duck.setScaleMinMax(-1,-1,1,1);
    this.duck.scale.setTo(0.5*window.devicePixelRatio, 0.5*window.devicePixelRatio);
    this.duck.anchor.setTo(0.5, 0); // top, center (so it flips evenly)
    this.duck.visible = false;

    this.add(this.duck);


    // Animations
    this.animationCounter = 0;

};

// Extend Phaser.Group
_Group.prototype = Object.create(Phaser.Group.prototype);
_Group.prototype.constructor = _Group;

_Group.prototype.getSprite = function() { return this.duck; }

_Group.prototype.reset = function() { 

    this.duck.visible = true;

}

_Group.prototype.start = function() {

    // this.sit();

}

/**
 * Automatically called by World.update
 */
_Group.prototype.update = function() {

    // Lock running an update to every other frame. (~15fps)
    if( this.frameCounter % 4 === 0 ) {

        // Mark when screen updates
        this.updateCounter++;
    }

    // Mark every actual frame
    this.frameCounter++;
};
