
Duck = function(game) { //x, y) {

    // Call parent constructor
    Phaser.Group.call(this, game, null, 'duck'); 

    // Store copy
    this.game = game;
    this.frameCounter = 0; // Keep count of actual framerate
    this.updateCounter = 0; // Keep count of visible framerate

    this.enableBody = false;

    // Events
    this.onDefendStart = new Phaser.Signal;

    // Umbrella
    this.umbrella = new Phaser.Sprite(this.game, 0, 0, 'duck_quest');
    this.umbrella.enableBody = false;
    this.umbrella.animations.frame = this.umbrella.animations.frameData.getFrameByName('umbrella-arm').index;
    this.umbrella.setScaleMinMax(-1,-1,1,1);
    this.umbrella.scale.setTo(0.5*window.devicePixelRatio, 0.5*window.devicePixelRatio);
    this.umbrella.anchor.setTo(0.5, 1); // top, center (so it flips evenly)
    this.umbrella.visible = false;

    this.add(this.umbrella);

    // Duck
    this.duck = new Phaser.Sprite(this.game, 0, 0, 'duck_quest');
    this.duck.enableBody = false;
    this.duck.setScaleMinMax(-1,-1,1,1);
    this.duck.scale.setTo(0.5*window.devicePixelRatio, 0.5*window.devicePixelRatio);
    this.duck.anchor.setTo(0.5, 1); // bottom, center (so it flips evenly)
    this.duck.visible = false;

    this.add(this.duck);


    // Animations
    this.walkCounter = 0;
    this.sitTimer = 0;
    this.isSitting = false;
    this.isDefending = false;
    this.scaleX = this.duck.scale.x;

};

// Extend Phaser.Group
Duck.prototype = Object.create(Phaser.Group.prototype);
Duck.prototype.constructor = Duck;

Duck.prototype.getSprite = function() { return this.duck; }

Duck.prototype.reset = function() { 

    this.onDefendStart.removeAll();
    
    this.umbrella.x = this.game.gridX(8);
    this.umbrella.y = this.game.gridX(-5);
    this.umbrella.visible = false;

    this.duck.visible = true;
    // this.duck.body.collideWorldBounds = true;
    // this.duck.body.allowGravity = false;

    this.isSitting = false;
    this.isDefending = false;
}

Duck.prototype.start = function() {

    this.sit();

    // this.duck.body.width = this.game.gridX(9);
    // this.duck.body.height = this.game.gridX(10);
    // this.duck.body.allowGravity = true;

}

Duck.prototype.sit = function() {
    if( this.isSitting ) 
        return;

    this.isSitting = true;

    // this.duck.animations.play('sit');
    this.duck.animations.frame = this.duck.animations.frameData.getFrameByName('duck-sitting').index;

    // move down
    // this.duck.y += this.game.gridX(2);
}

Duck.prototype.stand = function() {
    // Back up.
    if( this.isSitting ) {
        this.isSitting = false;
        // this.y -= this.game.gridX(2);
    }

    // this.duck.animations.play('stand');
    this.duck.animations.frame = this.duck.animations.frameData.getFrameByName('duck-standing').index;

    // Delay sitting
    this.sitTimer = 10;
}

Duck.prototype.walkLeft = function() {
    // Back up.
    if( this.isSitting ) {
        this.isSitting = false;
        // this.y -= this.game.gridX(2);
    }

    // Flip horizontally
    this.duck.scale.x = -0.5*window.devicePixelRatio;
    this.duck.animations.frame = 4;
    // Flip umbrella
    this.umbrella.scale.x = -0.5*window.devicePixelRatio;
    this.umbrella.x = -this.game.gridX(8);

    if( this.walkCounter > 7 )
        this.walkCounter = 0;

    if( this.walkCounter > 5 ) {
        this.duck.animations.frame = this.duck.animations.frameData.getFrameByName('duck-walk-4').index;
    }
    else if( this.walkCounter > 3 ) {
        this.duck.animations.frame = this.duck.animations.frameData.getFrameByName('duck-walk-3').index;
    }
    else if( this.walkCounter > 1 ) {
        this.duck.animations.frame = this.duck.animations.frameData.getFrameByName('duck-walk-2').index;
    }
    else {
        this.duck.animations.frame = this.duck.animations.frameData.getFrameByName('duck-walk-1').index;
    }

    this.walkCounter++;

    // Delay sitting
    this.sitTimer = 10;
}

Duck.prototype.walkRight = function() {
    // Back up.
    if( this.isSitting ) {
        this.isSitting = false;
        // this.y -= this.game.gridX(2);
    }

    // Correct any flipped horizontal scale
    this.duck.scale.x = 0.5*window.devicePixelRatio;
    // Ditto for umbrella
    this.umbrella.scale.x = 0.5*window.devicePixelRatio;
    this.umbrella.x = this.game.gridX(8);

    if( this.walkCounter > 7 )
        this.walkCounter = 0;

    if( this.walkCounter > 5 ) {
        this.duck.animations.frame = this.duck.animations.frameData.getFrameByName('duck-walk-4').index;
    }
    else if( this.walkCounter > 3 ) {
        this.duck.animations.frame = this.duck.animations.frameData.getFrameByName('duck-walk-3').index;
    }
    else if( this.walkCounter > 1 ) {
        this.duck.animations.frame = this.duck.animations.frameData.getFrameByName('duck-walk-2').index;
    }
    else {
        this.duck.animations.frame = this.duck.animations.frameData.getFrameByName('duck-walk-1').index;
    }

    this.walkCounter++;
    
    // Delay sitting
    this.sitTimer = 10;
}

Duck.prototype.defend = function() {
    // Back up.
    if( this.isSitting ) {
        // At least stand up...
        this.stand();
    }

    // Send signal
    this.onDefendStart.dispatch();

    this.isDefending = true;

    // Show umbrella
    this.umbrella.visible = true;

    // Delay sitting
    this.sitTimer = 10;
}

Duck.prototype.relax = function() {
    // Hide umbrella 
    if( ! this.isDefending )
        return;

    this.umbrella.visible = false;
    this.isDefending = false;
}

/**
 * Automatically called by World.update
 */
Duck.prototype.update = function() {

    // Lock running an update to every other frame. (~15fps)
    if( this.frameCounter % 4 === 0 ) {

        // Sit.
        if( this.sitTimer <= 0 )
            this.sit();

        this.sitTimer--;

        // Mark when screen updates
        this.updateCounter++;
    }

    // Mark every actual frame
    this.frameCounter++;
};
