
BackgroundStrobe = function(game) { //x, y) {

    // Call parent constructor
    Phaser.Group.call(this, game, null, 'bg_strobe'); //, x, y, 'title__gradient');

    this.bgEffectOn = false;
    this.bgEffectCount = 0;
    this.bgEffectFrames = 20;
    this.bg1;
    this.bg2;
    this.bg3;
    this.bg4;

    // Phaser.Group.prototype.update.call(this);
    // Phaser.Group.prototype.create.call(this);

    var bmd;

    // 1
    bmd = new Phaser.BitmapData(this.game, 'bg1', this.game.width, this.game.height);
    bmd.fill(255,255,148,1);
    this.bg1 = new Phaser.Sprite(this.game, 0, 0, bmd);

    // 2
    bmd = new Phaser.BitmapData(this.game, 'bg2', this.game.width, this.game.height);
    bmd.fill(208,208,102,1);
    this.bg2 = new Phaser.Sprite(this.game, 0, 0, bmd);

    // 3
    bmd = new Phaser.BitmapData(this.game, 'bg3', this.game.width, this.game.height);
    bmd.fill(148,148,64,1);
    this.bg3 = new Phaser.Sprite(this.game, 0, 0, bmd);

    // 4
    bmd = new Phaser.BitmapData(this.game, 'bg4', this.game.width, this.game.height);
    bmd.fill(102,102,37,1);
    this.bg4 = new Phaser.Sprite(this.game, 0, 0, bmd);

};

// Extend Phaser.Group
BackgroundStrobe.prototype = Object.create(Phaser.Group.prototype);
BackgroundStrobe.prototype.constructor = BackgroundStrobe;

BackgroundStrobe.prototype.reset = function() { 
    
    // Reset count for better animation
    this.bgEffectOn = false;
    this.bgEffectCount = 0;
    this.remove(this.bg1);
    this.remove(this.bg2);
    this.remove(this.bg3);
    this.remove(this.bg4);
}

BackgroundStrobe.prototype.setActive = function(val) {

    if( val === true ) {
        this.bgEffectOn = true;
        // Reset count for better animation
        this.bgEffectCount = 0;
    }
    else {
        this.reset();
    }

}

BackgroundStrobe.prototype.setStatic = function(bg) {

    // Turn off effect
    this.reset();

    // Display single color
    this.add(this[bg]);
    
}

/**
 * Automatically called by World.update
 */
BackgroundStrobe.prototype.update = function() {

    // Switch backgrounds every x frames
    if( this.bgEffectOn ) {
        
        // Reset
        if( this.bgEffectCount > this.bgEffectFrames*3-1 ) {
            this.bgEffectCount = 0;
        }

        // if( this.bgEffectCount === this.bgEffectFrames*3 ) {
        //     this.bgContainer.remove(this.bg3);
        //     this.bgContainer.add(this.bg4);
        //     this.bg4.visibility = true;
        //     // console.log("4");
        // }
        // else 
        if( this.bgEffectCount === this.bgEffectFrames*2 ) {
            this.remove(this.bg2);
            this.add(this.bg4);
            this.bg3.visibility = true;
            // console.log("3");
        }
        else if( this.bgEffectCount === this.bgEffectFrames ) {
            this.remove(this.bg3);
            this.add(this.bg2);
            this.bg2.visibility = true;
            // console.log("2");
        }
        else if( this.bgEffectCount === 0 ) {
            this.remove(this.bg4);
            this.add(this.bg3);
            this.bg1.visibility = true;
            // console.log("1");
        }

        this.bgEffectCount++;
    }

};