
Rain = function(game) {

    // Call parent constructor
    Phaser.Group.call(this, game, null, 'rain'); 

    // Disable physics on children
    this.enableBody = false;

    // Store copy
    this.game = game;
    this.frameCounter = 0; // Keep count of actual framerate
    this.updateCounter = 0; // Keep count of visible framerate
    this.createCounter = 0;
    this.jsfxCounter = 0;
    this.dropSoundCounter = 0;

    this.raining = false;

    // Object pool
    this.drops = [];
    var drop;
    for( var i = 0; i < 100; i++ ) {
        drop = new Drop(this.game, 0, 0);
        drop.enableBody = false;

        // Initial "dead" state
        drop.alive = false;

        // Listen for drop events
        drop.onDropFell.add(this.onDropFellHandler.bind(this))
        drop.onExplodeEnd.add(this.removeDrop.bind(this));

        // Add to pool
        this.drops[this.drops.length] = drop;
    }

    // Object pool
    this.addMultiple(this.drops, true); //silent

};

// Extend Phaser.Group
Rain.prototype = Object.create(Phaser.Group.prototype);
Rain.prototype.constructor = Rain;

Rain.prototype.reset = function() { 

    this.exists = false;
    this.raining = false;
    this.frameCounter = 0; 
    this.updateCounter = 0; 
    this.createCounter = 5;
    this.jsfxCounter = 0;
    this.dropSoundCounter = 0;

    // Remove all particles
    // this.removeAll(false, true); //destroy, silent

}

Rain.prototype.start = function() {

    this.exists = true;
    this.raining = true;

}

Rain.prototype.pause = function() {

    // Pause update
    this.exists = false;
    this.raining = false;

}

Rain.prototype.destroy = function() {

    this.drops = null;

    this.removeAll(true, true); //destroy, silent
    this.destroy(true);

}

Rain.prototype.onDropFellHandler = function(drop) {
    
    if( this.dropSoundCounter <= 0 ) {

        // Wait before playing this sound again
        this.dropSoundCounter = 2;

        // Play sound from one of the 3 channels
        if( this.jsfxCounter % 4 == 0 ) {
            this.game.jsfxSamples.drop.play();
        }
        else if( this.jsfxCounter % 4 == 1 ) {
            this.game.jsfxSamples2.drop.play();
        }
        else if( this.jsfxCounter % 4 == 2 ) {
            this.game.jsfxSamples3.drop.play();
        }
        else {
            this.game.jsfxSamples4.drop.play();
        }

        this.jsfxCounter += Math.floor(Math.random() * 3);
    }
}

Rain.prototype.removeDrop = function(drop) {

    drop.kill();

}

Rain.prototype.update = function() {

    // Lock running an update to every other frame. (~15fps)
    if( this.frameCounter % 4 === 0 ) {

        // Create now...
        if( this.raining ) {

            createCheck:
            if( this.createCounter <= 0 ) {

                // Get a drop from pool
                var drop = this.getFirstDead();

                if( ! drop ) 
                    break createCheck;
                
                // Activate it somewhere around this cameras view... (80 grid window)
                drop.reset(this.game.camera.x + this.game.gridX(Math.floor(Math.random() * 80)) - this.game.gridX(8), this.game.gridX(-10)); //x,y
                drop.resetAnimations();
                drop.start();

                // console.log("create drop at x:"+ drop.x);

                // Reset the timer 
                this.createCounter = 4 + Math.floor(Math.random() * 5); // 25-100
            }
            
            // Count down to create another
            this.createCounter--;
        }

        // Decrement wait time for jsfx sound
        this.dropSoundCounter--;

        // Mark when screen updates
        this.updateCounter++;
    }

    // Mark every actual frame
    this.frameCounter++;
};
