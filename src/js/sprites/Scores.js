
Scores = function(game) {

    // Call parent constructor
    Phaser.Group.call(this, game, null, 'scores'); 

    // Disable physics on children
    this.enableBody = false;

    // Store copy
    this.game = game;
    this.frameCounter = 0; // Keep count of actual framerate
    this.updateCounter = 0; // Keep count of visible framerate

    // Object pool
    this.scores = [];
    var score;
    for( var i = 0; i < 10; i++ ) { 
        score = new Phaser.BitmapText(
            this.game, 0, 0,
            'tom_128_light', '', 64*window.devicePixelRatio, 'center'
        );
        score.enableBody = false;

        // Initial "dead" state
        score.alive = false;

        // Add to pool
        this.scores[this.scores.length] = score;
    }

};

// Extend Phaser.Group
Scores.prototype = Object.create(Phaser.Group.prototype);
Scores.prototype.constructor = Scores;

Scores.prototype.reset = function() { 

    this.exists = false;
    this.frameCounter = 0; 
    this.updateCounter = 0;

    // Remove all particles
    this.removeAll(false, true); //destroy, silent

}

Scores.prototype.start = function() {

    // Object pool
    this.addMultiple(this.scores, true); //silent

    this.exists = true;

}

Scores.prototype.pause = function() {

    // Pause update
    this.exists = false;

}

Scores.prototype.destroy = function() {

    this.scores = null;

    this.removeAll(true, true); //destroy, silent
    this.destroy(true);

}

Scores.prototype.addScore = function(x,y,val) {

    console.log('add score: '+ val);

    var score = this.getFirstDead();

    if( ! score ) 
        return;
    
    // Activate it somewhere around this cameras view... (80 grid window)
    score.reset(x,y);
    score.text = val;
    score.life = 8;

}

Scores.prototype.removeScore = function(score) {

    score.kill();

}

Scores.prototype.update = function() {

    // Lock running an update to every other frame. (~15fps)
    if( this.frameCounter % 4 === 0 ) {

        this.iterate('alive', true, Phaser.Group.RETURN_TOTAL, function(activeScore) {

            // If still alive
            if( activeScore.life > 0 ) {

                // Float up animation
                if( Math.floor(activeScore.life / 2 % 2) === 0 ) {
                    activeScore.x -= this.game.gridX(Math.floor(Math.random()*2));
                    activeScore.y -= this.game.gridX(Math.floor(Math.random()*2));
                }
                else {
                    activeScore.x += this.game.gridX(Math.floor(Math.random()*2));
                    activeScore.y -= this.game.gridX(Math.floor(Math.random()*2));
                }
            }
            else {
                this.removeScore(activeScore);

                return;
            }

            activeScore.life--;

        }.bind(this));

    }

    // Mark every actual frame
    this.frameCounter++;
};
