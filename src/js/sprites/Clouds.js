
Clouds = function(game) {

    // Call parent constructor
    Phaser.Group.call(this, game, null, 'rain'); 

    // Disable physics on children
    this.enableBody = false;

    // Store copy
    this.game = game;
    this.frameCounter = 0; // Keep count of actual framerate
    this.updateCounter = 0; // Keep count of visible framerate
    this.createCounter = 0;
    this.numClouds = 0;
    this.animating = false;

    // Object pool
    this.clouds = [];
    var cloud;
    for( var i = 0; i < 6; i++ ) {
        // Make two of each
        // console.log("making cloud "+ 'cloud-'+ Math.ceil((i+1)/2));
        cloud = new Cloud(this.game, 0, 0, 'duck_quest', 'cloud-'+ Math.ceil((i+1)/2));
        cloud.enableBody = false;

        // Initial "dead" state
        cloud.kill();

        // Add to pool
        this.clouds[this.clouds.length] = cloud;
    }

};

// Extend Phaser.Group
Clouds.prototype = Object.create(Phaser.Group.prototype);
Clouds.prototype.constructor = Clouds;

Clouds.prototype.reset = function() { 

    this.exists = false;
    this.frameCounter = 0; 
    this.updateCounter = 0; 
    this.createCounter = 5;
    this.numClouds = 0;
    this.animating = false;

    // Remove all particles
    this.removeAll(false, true); //destroy, silent

}

Clouds.prototype.start = function() {

    // Object pool
    this.addMultiple(this.clouds, true); //silent

    this.exists = true;
    this.animating = true;

}

Clouds.prototype.pause = function() {

    // Pause update
    this.exists = false;
    this.animating = false;

}

Clouds.prototype.destroy = function() {

    this.clouds = null;

    this.removeAll(true, true); //destroy, silent
    this.destroy(true);

}

Clouds.prototype.removeCloud = function(cloud) {

    cloud.kill();
    this.numClouds--;

}

Clouds.prototype.update = function() {

    // Lock running an update to every other frame. (~15fps)
    if( this.frameCounter % 4 === 0 ) {

        if( this.animating ) {

            // Only 3 max
            if( this.numClouds < 5 ) {

                createCheck:
                if( this.createCounter <= 0 ) {

                    // Get a cloud from pool
                    var cloud = this.getFirstDead();

                    if( ! cloud ) 
                        break createCheck;
                    
                    // Activate it somewhere around this cameras view... (80 grid window)
                    cloud.reset(this.game.gridX(90), this.game.gridX(-12+Math.floor(Math.random()*16))); //x,y
                    cloud.resetAnimations();
                    cloud.start();

                    this.numClouds++;

                    // console.log("create cloud at x:"+ cloud.x);

                    // Reset the timer 
                    this.createCounter = 60 + Math.floor(Math.random() * 120); 
                }
            }

            // Bounds check
            this.iterate('alive', true, Phaser.Group.RETURN_TOTAL, function(activeCloud) {

                if( activeCloud.x < this.game.gridX(-64) ) {
                    this.removeCloud(activeCloud);
                    
                    return;
                }

                // Not sure why we have to manually update each drop...
                activeCloud.update();

            }.bind(this));
            
            // Count down to create another
            this.createCounter--;
        }

        // Mark when screen updates
        this.updateCounter++;
    }

    // Mark every actual frame
    this.frameCounter++;
};
