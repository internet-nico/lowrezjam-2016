
TopFlowers = function(game) { 

    // Call parent constructor
    Phaser.Group.call(this, game, null, 'top_flowers'); 

    // Store copy
    this.game = game;
    this.frameCounter = 0; // Keep count of actual framerate
    this.updateCounter = 0; // Keep count of visible framerate
    this.addCounter = 0;

    this.enableBody = false;

    // Object pool
    this.flowers = [];
    var flower;

    // 1
    flower = new Phaser.Sprite(this.game, 0, 0, 'duck_quest', 'flower-top-1');
    flower.enableBody = false;
    flower.setScaleMinMax(0.5,0.5,1,1);
    flower.scale.setTo(0.5*window.devicePixelRatio, 0.5*window.devicePixelRatio);
    flower.anchor.setTo(0, 1); // bottom left
    flower.alive = false;
    this.flowers[this.flowers.length] = flower;

    // 2
    flower = new Phaser.Sprite(this.game, 0, 0, 'duck_quest', 'flower-top-2');
    flower.enableBody = false;
    flower.setScaleMinMax(0.5,0.5,1,1);
    flower.scale.setTo(0.5*window.devicePixelRatio, 0.5*window.devicePixelRatio);
    flower.anchor.setTo(0, 1); // bottom left
    flower.alive = false;
    this.flowers[this.flowers.length] = flower;
    
    // 3
    flower = new Phaser.Sprite(this.game, 0, 0, 'duck_quest', 'flower-top-3');
    flower.enableBody = false;
    flower.setScaleMinMax(0.5,0.5,1,1);
    flower.scale.setTo(0.5*window.devicePixelRatio, 0.5*window.devicePixelRatio);
    flower.anchor.setTo(0, 1); // bottom left
    flower.alive = false;
    this.flowers[this.flowers.length] = flower;

    // Object pool
    this.addMultiple(this.flowers, true); //silent

};

// Extend Phaser.Group
TopFlowers.prototype = Object.create(Phaser.Group.prototype);
TopFlowers.prototype.constructor = TopFlowers;

TopFlowers.prototype.reset = function() { 

    this.addCounter = 0;

    // this.removeAll(true, true);

}

TopFlowers.prototype.start = function() {

    // Object pool
    // this.addMultiple(this.flowers, true); //silent

}

TopFlowers.prototype.destroy = function() {
    
    this.flowers = null;
    this.removeAll(true, true); //destroy, silent
    this.destroy(true);

}

TopFlowers.prototype.addFlower = function(x) {

    var flower = this.getRandom();

    if( ! flower )
        return;

    // Default x
    if( ! x || typeof x === 'undefined' ) 
        x = this.game.camera.x + this.game.gridX(65);

    flower.revive()
    flower.reset(x, this.game.gridX(52));

}

TopFlowers.prototype.removeFlower = function(flower) {

    flower.kill();

    // Add another soon
    this.addCounter = 5 + Math.floor(Math.random() * 20);

}


/**
 * Automatically called by World.update
 */
TopFlowers.prototype.update = function() {

    // Lock running an update to every other frame. (~15fps)
    if( this.frameCounter % 4 === 0 ) {

        // Check distance on flowers
        this.iterate('alive', true, Phaser.Group.RETURN_TOTAL, function(activeFlower) {
            
            // Check bounds
            if( activeFlower.x < this.game.camera.x - this.game.gridX(16) ) {
                this.removeFlower(activeFlower);
            }

        }.bind(this));

        if( this.addCounter > 0 ) {

            // Count down to adding new flower
            this.addCounter--;

            if( this.addCounter === 0 ) {
                // Add one
                this.addFlower();
            }
        }

        // Mark when screen updates
        this.updateCounter++;
    }

    // Mark every actual frame
    this.frameCounter++;
};
