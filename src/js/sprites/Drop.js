
Drop = function(game, x, y) {

    // Call parent constructor
    Phaser.Sprite.call(this, game, x, y, 'duck_quest', 'drop-1'); 

    // Store copy
    this.game = game;
    this.updateCounter;
    this.frameCounter;

    // Events
    this.onDropFell = new Phaser.Signal;
    this.onExplodeEnd = new Phaser.Signal;

    // View
    this.setScaleMinMax(0.5,0.5,1,1);
    this.scale.setTo(0.5*window.devicePixelRatio, 0.5*window.devicePixelRatio);
    // this.anchor.setTo(0.5, 0); // top, center (so it flips evenly)
    this.visible = false;

    // Animating
    this.falling = false;
    this.animating = false;
    this.animationIndex = 1;
    this.animateCounter = 0;
    this.startY;
    this.endY;

};

// Extend Phaser.Sprite
Drop.prototype = Object.create(Phaser.Sprite.prototype);
Drop.prototype.constructor = Drop;

Drop.prototype.resetAnimations = function() { 

    this.visible = false;

    this.falling = false;
    this.animating = false;
    this.updateCounter = 0;
    this.frameCounter = 0;
    this.animationIndex = 1;
    this.animateCounter = 0;
    this.animations.frame = this.animations.frameData.getFrameByName('drop-'+ this.animationIndex).index;

    // New fall path
    this.startY = this.y;
    this.endY = this.game.gridX(58 + Math.floor(Math.random()*5));
}

Drop.prototype.start = function() {

    // this.sit();
    this.visible = true;

    this.falling = true;

}

Drop.prototype.explode = function() {

    // Start update
    this.falling = false;
    // Explode animation
    this.animating = true;

}

/**
 * Automatically called by World.update
 */
Drop.prototype.update = function() {

    // Lock running an update to every other frame. (~15fps)
    if( this.frameCounter % 4 === 0 ) {

        // Currently falling...
        if( this.falling ) {

            var totalSteps = 30;
            var gridWidth = 8 * window.devicePixelRatio; // 8px on 64x64 grid

            if( this.animateCounter < totalSteps ) {

                // Start + totalDistance * ratio
                this.y = this.startY + Math.abs(this.endY - this.startY) * this.game.ease.p2EaseRatio(2, (this.animateCounter / totalSteps));
                this.y -= this.y % gridWidth;

                // Mark as animated
                this.animateCounter++;
            }
            else {
                // Dead, fell on ground
                this.onDropFell.dispatch(this);

                this.explode();
            }

        }

        // Currently Exploding...
        if( this.animating ) {

            this.animationIndex++;

            // Skip this frame...
            // if( this.animationIndex === 2 )
                // this.animationIndex = 3;

            switch( this.animationIndex ) {
                case 3 : 
                    this.y -= this.game.gridX(1);
                    this.x -= this.game.gridX(1);
                break;
                case 4 : 
                    this.y -= this.game.gridX(3);
                    this.x -= this.game.gridX(1);
                break;
                case 5 : 
                    
                    // That's all folks...
                    this.onExplodeEnd.dispatch(this);

                    return;
                break;
            }

            // Show animation frame
            // console.log('showing frame '+ this.animationIndex);
            this.animations.frame = this.animations.frameData.getFrameByName('drop-'+ this.animationIndex).index;

            // Currently exploding... no need to check bounds
            return;
        }

        // Mark when screen updates
        this.updateCounter++;
    }

    // Mark every actual frame
    this.frameCounter++;
};
