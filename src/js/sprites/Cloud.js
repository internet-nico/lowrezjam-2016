
Cloud = function(game, x, y, key, frame) {

    // Call parent constructor
    console.log(key, frame);
    Phaser.Sprite.call(this, game, x, y, key, frame); 

    // Store copy
    this.game = game;
    this.updateCounter;
    this.frameCounter;
    this.animateCounter;

    // View
    this.setScaleMinMax(0.5,0.5,1,1);
    this.scale.setTo(0.5*window.devicePixelRatio, 0.5*window.devicePixelRatio);
    // this.anchor.setTo(0.5, 0); // top, center (so it flips evenly)
    this.visible = false;
    this.alpha = 0.5;

    // Animating
    this.moving = false;
    this.startX;
    this.endX;
    this.totalSteps;

};

// Extend Phaser.Sprite
Cloud.prototype = Object.create(Phaser.Sprite.prototype);
Cloud.prototype.constructor = Cloud;

Cloud.prototype.resetAnimations = function() { 

    this.visible = false;

    this.moving = false;
    this.updateCounter = 0;
    this.frameCounter = 0;
    this.animateCounter = 0;

    // New fall path
    this.startX = this.x 
    this.endX = this.game.gridX(-70);
    this.totalSteps = 100; //80 + Math.floor(Math.random() * 160);

    // console.log("startX", this.startX, "endX", this.endX);
}

Cloud.prototype.start = function() {

    // this.sit();
    this.visible = true;

    this.moving = true;

}

/**
 * Automatically called by World.update
 */
Cloud.prototype.update = function() {

    // Lock running an update to every other frame. (~15fps)
    if( this.frameCounter % 4 === 0 ) {

        // Currently moving...
        if( this.moving ) {

            var gridWidth = 8 * window.devicePixelRatio; // 8px on 64x64 grid

            if( this.animateCounter < this.totalSteps ) {

                // Start + totalDistance * ratio
                this.x = this.startX - Math.abs(this.startX - this.endX) * (this.animateCounter / this.totalSteps);
                this.x -= this.x % gridWidth;

                // console.log("cloud.x "+ this.x);

                // Mark as animated
                this.animateCounter++;
            }
            else {
                // Dead
            }

        }

        // Mark when screen updates
        this.updateCounter++;
    }

    // Mark every actual frame
    this.frameCounter++;
};
