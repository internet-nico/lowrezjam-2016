
BasicGame.Level0Screen = function() {

    // Public signals
    this.onScreenInitialized = new Phaser.Signal;

    // Screen
    this.game;
    this.id = "level_0";
    this.container;
    this.isScreenInitialized = false;
    this.updateBeforeTransitionIn;
    this.frameCounter = 0; // Keep count of actual framerate
    this.updateCounter = 0; // Keep count of visible framerate

    // Containers
    this.backContainer;
    this.midContainer;
    this.foreContainer;
    this.uiContainer;

    // Game
    this.score = 0;
    this.levelOver;
    this.levelStarted;

    // Tutorial Level

};

BasicGame.Level0Screen.prototype = {

    create : function( game, container ) {

        console.log("create "+ this.id +" screen");

        this.game = game;

        // Main container
        this.container = new Phaser.Group(this.game, container);

        // Micro containers
        this.backContainer = new Phaser.Group(this.game, this.container);
        this.midContainer = new Phaser.Group(this.game, this.container);
        this.foreContainer = new Phaser.Group(this.game, this.container);
        this.uiContainer = new Phaser.Group(this.game, this.container);

        // Make stuff
        

        // Reset positions, visibility, physics, whatever
        this.reset();

        // Finished creating, mark screen init 
        this.isScreenInitialized = true;
        this.onScreenInitialized.dispatch();
    },

    reset : function() {

        console.log("game screen reset");

        // Remove all buttons
        // this.uiContainer.remove( this.menuButton );
        // this.menuButton.visible = false;

        // Reset
        this.score = 0;
        this.levelOver = false;
        this.levelStarted = false;

    },

    // Screen has been created and transitioned in 
    playScreen : function() {
        
        console.log("game screen play");

        // Add listeners
        // this.menuButton.events.onInputUp.add();

        // Start... or move to wait for an input or something
        if ( ! this.levelStarted ) {
            this.start();
        }

    },

    pauseScreen : function() {

        console.log("game screen pause");

        // Remove listeners
        // this.menuButton.events.onInputUp.removeAll();
    },

    // After the user "flaps" or some other input
    start : function() {

        console.log("level away!");

        // Start update process
        this.levelStarted = true;

    },

    updateBeforeTransitionIn : function() {

    },

    levelWinHandler : function() {

        // Write current score
        this.game.currentScore = this.score;

    },

    levelLoseHandler : function() {

        console.log("game over :(");
        console.log("score: "+ this.score);

        this.levelOver = true;

        // Write current score
        this.game.currentScore = this.score;

    },

    update : function() {
        //console.log("game screen update");

        
        // Lock running an update to every fourth frame. (~15fps)
        if( this.frameCounter % 4 === 0 ) {

            if( this.levelStarted ) {

                if( this.levelOver ) {
                    // Currently dead. Viewing their score, waiting to reset.

                    console.log("dead.");

                }
                else {
                    // Playing...

                    // console.log("playing...");

                }

            }
            else {
                // Game is reset, waiting for user to begin play
            }

            // Mark when screen updates
            this.updateCounter++;
        }

        // Mark every actual frame
        this.frameCounter++;
    },

    render : function() {

    },

    destroy : function() {

    }

};
