
BasicGame.TitleScreen = function() {

    // Public signals
    this.onScreenInitialized = new Phaser.Signal;
    this.onPlayButtonClicked = new Phaser.Signal;

    this.game;
    this.id = "title";
    this.container;
    this.isScreenInitialized = false;

    this.backContainer;
    this.midContainer;
    this.foreContainer;

    // Sprites
    this.duck;
    this.gradient;
    this.duckTitle;

    this.frameCounter = 0; // Keep count of actual framerate
    this.updateCounter = 0; // Keep count of visible framerate

    this.animationsInit = false;
    
    this.duckAnimate;
    this.duckAnimateCounter;
    this.gradientAnimate;
    this.gradientAnimateCounter;
    this.titleAnimate;
    this.titleAnimateCounter;
    this.pushInfoAnimate;
    this.pushInfoAnimateCounter;

    this.pushInfo;
    this.enterKey;
    this.spaceKey;
    this.startKeyPressed = false; 
    this.strobe;

};

BasicGame.TitleScreen.prototype = {

    create : function( game, gameContainer ) {

        console.log("create "+ this.id +" screen");

        // Hold a copy of the game
        this.game = game;

        // Add this screens container as child of the main container
        this.container = new Phaser.Group(this.game, gameContainer);

        // Background groups
        this.backContainer = new Phaser.Group(this.game, this.container);
        this.midContainer = new Phaser.Group(this.game, this.container);
        this.foreContainer = new Phaser.Group(this.game, this.container);

        // Background strobe
        this.strobe = new BackgroundStrobe(this.game); //, 0, 0);
        this.backContainer.add(this.strobe);

        // Gradient
        this.gradient = new Phaser.Sprite(this.game, 0, 0, 'title__gradient');
        this.gradient.setScaleMinMax(0.5,0.5,1,1);
        this.gradient.scale.setTo(0.5*window.devicePixelRatio, 0.5*window.devicePixelRatio);
        this.midContainer.addChild(this.gradient);

        // Duck
        this.duck = new Phaser.Sprite(this.game, 0, 0, 'title__duck');
        this.duck.setScaleMinMax(0.5,0.5,1,1);
        this.duck.scale.setTo(0.5*window.devicePixelRatio, 0.5*window.devicePixelRatio);
        this.foreContainer.addChild(this.duck);

        // Title
        this.duckTitle = new Phaser.Sprite(this.game, 0, 0, 'title__duck-quest');
        this.duckTitle.setScaleMinMax(0.5,0.5,1,1);
        this.duckTitle.scale.setTo(0.5*window.devicePixelRatio, 0.5*window.devicePixelRatio);
        this.midContainer.addChild(this.duckTitle);

        // Push Start
        this.pushInfo = new Phaser.BitmapText(
            this.game, 0, 0,
            'visitor_160_light', 'Push Start', 80*window.devicePixelRatio, 'center'
        );
        this.foreContainer.add(this.pushInfo);

        // Test grid
        // this.container.add(new Phaser.Sprite(this.game, 0, 0, 'test_grid'));

        // Reset positions for everything that moves
        this.reset();

        // Keys
        this.enterKey = this.game.input.keyboard.addKey(Phaser.KeyCode.ENTER);
        this.spaceKey = this.game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);

        // Finished creating, mark screen init 
        this.isScreenInitialized = true;
        this.onScreenInitialized.dispatch();
    },

    reset : function() {

        this.frameCounter = 0;
        this.updateCounter = 0;
        this.startKeyPressed = false;

        this.strobe.reset();
        this.strobe.setStatic('bg4');

        // Update all animations
        this.animationsInit = false;

        // Move duck offscreen
        this.duck.x = this.game.gridX(-50);
        this.duck.y = this.game.gridX(20);
        this.duckAnimate = false;
        this.duckAnimateCounter = 0;

        // Title
        this.duckTitle.x = this.game.gridX(9);
        this.duckTitle.y = this.game.gridX(2);
        this.duckTitle.alpha = 0;
        this.titleAnimate = false;
        this.titleAnimateCounter = 0;

        // Gradient
        this.gradient.x = 0;
        this.gradient.y = this.game.gridX(-9);
        this.gradient.alpha = 0;
        this.gradientAnimate = false;
        this.gradientAnimateCounter = 0;

        // Push Start
        this.pushInfo.x = this.game.gridX(2);
        this.pushInfo.y = this.game.gridX(55);
        this.pushInfo.alpha = 0;
        this.pushInfoAnimate = false;
        this.pushInfoAnimateCounter = 0;

    },

    startKeyPressedHandler : function() {

        if( this.startKeyPressed )
            return;

        this.startKeyPressed = true;

        // Stop blinking text
        this.pushInfoAnimate = false;
        this.pushInfo.alpha = 0;

        // Start crazy bg strobe
        this.strobe.setActive(true);

        setTimeout(function() {

            // Transition to game
            this.onPlayButtonClicked.dispatch();

        }.bind(this), 3200);

        // Sound
        // this.game.jsfxSamples.noise.play();
        this.game.jsfxSamples.explode.play();
    },

    playScreen : function() {

        console.log(this.id +" play");

        // Enable inputs
        this.enterKey.onDown.add(this.startKeyPressedHandler, this);
        this.spaceKey.onDown.add(this.startKeyPressedHandler, this);

        // First time? 
        if( ! this.animationsInit ) {

            // KICK OFF THE SEQUENCE!
            setTimeout(function() {
                
                // Start duck animation
                this.duckAnimate = true;

                setTimeout(function() {

                    // Start gradient animation
                    this.gradientAnimate = true;

                    setTimeout(function() {
                        
                        // Display Title
                        this.titleAnimate = true;

                        setTimeout(function() {

                            // Display Push info
                            this.pushInfoAnimate = true;

                        }.bind(this), 1000);
                    }.bind(this), 1400);
                }.bind(this), 1250);
            }.bind(this), 20);

            // Don't do this again until reset()
            this.animationsInit = true;
        }

    },

    pauseScreen : function() {
        
        console.log(this.id +" pause");

        // Pause custom sprites (static to bg4)
        // this.strobe.setActive(false);
        this.strobe.setStatic('bg4');

        // Disable inputs
        this.enterKey.onDown.removeAll(this);
        this.spaceKey.onDown.removeAll(this);

    },

    update : function() {
        // console.log(this.id +" update"); //, now: "+ this.game.time.now, this.frameCounter);

        // Lock running an update to every fourth frame. (~15fps)
        if( this.frameCounter % 4 === 0 ) {

            // Bob taco...
            // this.tacoPixel.y = 250 + 20 * Math.sin(this.frameCounter / 20);        

            // if( this.blinkText ) {
            //     if( Math.floor(this.frameCounter / 20) % 2 ) {
            //         this.pushInfo.alpha = 0;
            //     }
            //     else {
            //         this.pushInfo.alpha = 1;
            //     }
            // }

            if( this.duckAnimate ) {

                // over x total updates
                // the duck should move from progress 0 (-100px) to progress 1 (20px)
                // what is his position for frame x?

                // currentStep++
                // distance per step: (max-min)/steps
                // current progress: currentStep/totalSteps
                // currentX = currentStep * (distancePerStep)
                // pos(currentX) = (minX + maxX) * getEaseRatio(currentProgress)
                // pos.x -= pos.x % gridWidth

                var totalSteps = 100;
                var startX = this.game.gridX(-50);
                var endX = this.game.gridX(10);
                var gridWidth = 8 * window.devicePixelRatio; // 8px on 64x64 grid
                // var totalDistance = Math.abs(endX - startX);
                // var distancePerStep = (endX-startX)/totalSteps;
                // var progress = this.duckAnimateCounter / totalSteps;

                // if( this.duck.x < this.game.gridX(2) ) {
                if( this.duckAnimateCounter < totalSteps ) {

                    // Start + totalDistance * ratio
                    this.duck.x = startX + Math.abs(endX - startX) * this.game.ease.p2EaseRatio(1, (this.duckAnimateCounter / totalSteps));
                    this.duck.x -= this.duck.x % gridWidth;

                    // Mark as animated
                    this.duckAnimateCounter++;
                }
                else {
                    // Done animating
                    this.duck.x = endX;
                    this.duckAnimate = false;
                }
            }

            if( this.gradientAnimate ) {

                var totalSteps = 50;
                var startX = 0;
                var endX = 1;

                if( this.gradientAnimateCounter < totalSteps ) {
                    
                    this.gradient.alpha = startX + Math.abs(endX - startX) * this.game.ease.bounceOutRatio((this.gradientAnimateCounter / totalSteps));

                    // Mark as animated
                    this.gradientAnimateCounter++;
                }
                else {
                    // Dont animating
                    this.gradient.alpha = endX;
                    this.gradientAnimate = false;
                }
            }

            if( this.titleAnimate ) {

                // 5 steps, animate alpha linearly

                // Slowly... every 3 frames that update
                if( this.updateCounter % 3 === 0 ) {

                    if( this.titleAnimateCounter < 6 ) {

                        this.duckTitle.alpha = (this.titleAnimateCounter+1)/6;

                        this.titleAnimateCounter++;
                    }
                    else {
                        // Done animating
                        this.duckTitle.alpha = 1;
                        this.titleAnimate = false;
                    }
                }
            }

            if( this.pushInfoAnimate ) {

                if( Math.floor(this.frameCounter / 20) % 3 === 0 ) {
                    this.pushInfo.alpha = 0;
                }
                else {
                    this.pushInfo.alpha = 1;
                }
            }

            // Mark when screen updates
            this.updateCounter++;
        }

        // Mark every actual frame
        this.frameCounter++;
    },

    render : function() {

    },

    destroy : function () {

    }

};
