
BasicGame.LevelTestScreen = function() {

    // Public signals
    this.onScreenInitialized = new Phaser.Signal;

    // Screen
    this.game;
    this.id = "level_test";
    this.container;
    this.isScreenInitialized = false;
    this.updateBeforeTransitionIn;
    this.timeCounter = 0;

    // Game
    this.score = 0;
    this.levelOver;
    this.levelStarted;

    // Tutorial Level
    this.player;
    this.platforms;
    // this.cursors;

    this.stars;
    this.scoreText;

};

BasicGame.LevelTestScreen.prototype = {

    create : function( game, container ) {

        console.log("create "+ this.id +" screen");

        // Hold a copy of the game
        this.game = game;

        // Add this screens container as child of the main container
        this.container = new Phaser.Group(this.game, container);

        //  We're going to be using physics, so enable the Arcade Physics system
        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        //  A simple background for our game
        var sky = new Phaser.Sprite(this.game, 0, 0, 'sky');
        sky.setScaleMinMax(1,1,2,2);
        sky.scale.setTo(1*window.devicePixelRatio, 1*window.devicePixelRatio);
        this.container.add(sky);

        //  The platforms group contains the ground and the 2 ledges we can jump on
        this.platforms = new Phaser.Group(this.game, this.container);

        //  We will enable physics for any object that is created in this group
        this.platforms.enableBody = true;

        // Here we create the ground.
        var ground = this.platforms.create(0, game.world.height - (64*window.devicePixelRatio), 'ground');
        ground.setScaleMinMax(2,2,4,4);
        ground.scale.setTo(2*window.devicePixelRatio, 2*window.devicePixelRatio);
        //  This stops it from falling away when you jump on it
        ground.body.immovable = true;

        //  Now let's create two ledges
        var ledge = this.platforms.create(400*window.devicePixelRatio, 400*window.devicePixelRatio, 'ground');
        ledge.setScaleMinMax(1,1,2,2);
        ledge.scale.setTo(1*window.devicePixelRatio, 1*window.devicePixelRatio);
        ledge.body.immovable = true;

        ledge = this.platforms.create(-150*window.devicePixelRatio, 250*window.devicePixelRatio, 'ground');
        ledge.setScaleMinMax(1,1,2,2);
        ledge.scale.setTo(1*window.devicePixelRatio, 1*window.devicePixelRatio);
        ledge.body.immovable = true;

        // // The player and its settings
        // this.player = new Phaser.Sprite(32*window.devicePixelRatio, game.world.height - 300*window.devicePixelRatio, 'dude');
        // this.container.add(this.player);

        // //  We need to enable physics on the this.player
        // this.game.physics.arcade.enable(this.player);

        // //  this.Player physics properties. Give the little guy a slight bounce.
        // this.player.body.bounce.y = 0.2*window.devicePixelRatio;
        // this.player.body.gravity.y = 300*window.devicePixelRatio;
        // this.player.body.collideWorldBounds = true;

        // //  Our two animations, walking left and right.
        // this.player.animations.add('left', [0, 1, 2, 3], 10, true);
        // this.player.animations.add('right', [5, 6, 7, 8], 10, true);

        //  Finally some stars to collect
        this.stars = new Phaser.Group(this.game, this.container);

        //  We will enable physics for any star that is created in this group
        this.stars.enableBody = true;

        //  Here we'll create 12 of them evenly spaced apart
        for (var i = 0; i < 12; i++)
        {
            //  Create a star inside of the 'this.stars' group
            var star = this.stars.create(i * 70*window.devicePixelRatio, 0, 'heart');

            star.setScaleMinMax(0.5,0.5,1,1);
            star.scale.setTo(0.5*window.devicePixelRatio, 0.5*window.devicePixelRatio);

            //  Let gravity do its thing
            star.body.gravity.y = ((Math.random()*200)+100);// * window.devicePixelRatio;

            //  This just gives each star a slightly random bounce value
            star.body.bounce.y = (0.7 + Math.random() * 0.2);// * window.devicePixelRatio;
        }

        // The score
        // this.scoreText = new Phaser.Text(16*window.devicePixelRatio, 16*window.devicePixelRatio, 'score: 0', { fontSize: '32px', fill: '#000' }, this.container);

        // Our controls.
        // this.cursors = this.game.input.keyboard.createCursorKeys();




        // Reset positions and visibility
        // this.reset();

        // Start with physics paused
        this.game.physics.arcade.isPaused = true;

        // Finished creating, mark screen init 
        this.isScreenInitialized = true;
        this.onScreenInitialized.dispatch();
    },

    // Screen has been created and transitioned in 
    playScreen : function() {
        
        console.log("game screen play");

        // Start physics
        this.game.physics.arcade.isPaused = false;

        // Add listeners
        // this.menuButton.events.onInputUp.add();

        // Start... or move to wait for an input or something
        if ( ! this.levelStarted ) {
            this.start();
        }

    },

    pauseScreen : function() {

        console.log("game screen pause");

        // Pause physics
        this.game.physics.arcade.isPaused = true;

        // Remove listeners
        // this.menuButton.events.onInputUp.removeAll();
    },

    reset : function() {

        console.log("game screen reset");

        // Remove all buttons
        // this.uiContainer.remove( this.menuButton );
        // this.menuButton.visible = false;

        // Reset
        this.score = 0;
        this.levelOver = false;
        this.levelStarted = false;

    },

    // After the user "flaps" or some other input
    start : function() {

        console.log("level away!");

        // Start update process
        this.levelStarted = true;

    },

    updateBeforeTransitionIn : function() {

    },

    levelWinHandler : function() {

        // Write current score
        this.game.currentScore = this.score;

    },

    levelLoseHandler : function() {

        console.log("game over :(");
        console.log("score: "+ this.score);

        this.levelOver = true;

        // Write current score
        this.game.currentScore = this.score;

    },

    update : function() {

        if( ! this.game.physics.arcade.isPaused ) {
            //console.log("game screen update");

            //console.log(this.game.time.time +", "+ this.game.time.now +", "+ this.taco.y);

            if( this.levelStarted ) {

                if( this.levelOver ) {
                    // Currently dead. Viewing their score, waiting to reset.

                    console.log("dead.");

                }
                else {
                    // Playing...

                    //  Collide the player and the stars with the platforms
                    // this.game.physics.arcade.collide(this.player, this.platforms);
                    this.game.physics.arcade.collide(this.stars, this.platforms);

                    // //  Checks to see if the this.player overlaps with any of the this.stars, if he does call the collectStar function
                    // this.game.physics.arcade.overlap(this.player, this.stars, this.collectStar, null, this);

                    // //  Reset the this.players velocity (movement)
                    // this.player.body.velocity.x = 0;

                    // if (this.cursors.left.isDown)
                    // {
                    //     //  Move to the left
                    //     this.player.body.velocity.x = -150;

                    //     this.player.animations.play('left');
                    // }
                    // else if (this.cursors.right.isDown)
                    // {
                    //     //  Move to the right
                    //     this.player.body.velocity.x = 150;

                    //     this.player.animations.play('right');
                    // }
                    // else
                    // {
                    //     //  Stand still
                    //     this.player.animations.stop();

                    //     this.player.frame = 4;
                    // }
                    
                    // //  Allow the this.player to jump if they are touching the ground.
                    // if (this.cursors.up.isDown && this.player.body.touching.down)
                    // {
                    //     this.player.body.velocity.y = -350;
                    // }

                }

            }
            else {
                // Game is reset, waiting for user to begin play
            }

            // Backgrounds...
            // this.updateClouds();

            // Update internal clock
            this.timeCounter++;
        }
    },

    collectStar : function(player, star) {
        
        // Removes the star from the screen
        star.kill();

        //  Add and update the score
        this.score += 10;
        this.scoreText.text = 'Score: ' + this.score;

    },

    render : function() {

    },

    destroy : function() {

    }

};
