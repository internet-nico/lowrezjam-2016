
BasicGame.Level1Screen = function() {

    // Public signals
    this.onScreenInitialized = new Phaser.Signal;

    // Screen
    this.game;
    this.gameContainer;
    this.id = "level_1";
    this.container;
    this.isScreenInitialized = false;
    this.updateBeforeTransitionIn;
    this.frameCounter = 0; // Keep count of actual framerate
    this.updateCounter = 0; // Keep count of visible framerate

    // Containers
    this.backContainer;
    this.midContainer;
    this.foreContainer;
    this.uiContainer;

    // Game
    // this.GRAVITY = 100;
    this.score = 0;
    this.levelOver;
    this.levelStarted;

    // Inputs
    this.leftArrow;
    this.rightArrow;
    this.spaceKey;
    this.spaceKeyNeedsReset;
    
    // Level 1
    this.duck;
    this.strobe;
    this.ground;
    this.rain;
    this.scores;
    this.jsfxCounter;
    this.collectSoundCounter;
    this.topFlowers;
    this.bottomFlowers;
    this.shakeTimer;
    this.defendTimer;
    this.defendLine;
    this.defendLimit = 20;
    this.levelWidth;
    this.clouds;
    this.flod;

};

BasicGame.Level1Screen.prototype = {

    create : function( game, gameContainer ) {

        console.log("create "+ this.id +" screen");

        this.game = game;
        this.gameContainer = gameContainer;
        this.levelWidth = this.game.gridX(800);

        // Main container
        this.container = new Phaser.Group(this.game, gameContainer);

        // Micro containers
        this.backContainer = new Phaser.Group(this.game, this.container);
        this.midContainer = new Phaser.Group(this.game, this.container);
        this.foreContainer = new Phaser.Group(this.game, this.container);
        this.uiContainer = new Phaser.Group(this.game, this.container);

        // Make stuff

        // Background strobe
        this.strobe = new BackgroundStrobe(this.game);
        this.strobe.fixedToCamera = true;
        this.backContainer.add(this.strobe);

        // Gradient
        var gradient = new Phaser.Sprite(this.game, 0, 0, 'level__gradient');
        gradient.setScaleMinMax(0.5,0.5,1,1);
        gradient.scale.setTo(0.5*window.devicePixelRatio, 0.5*window.devicePixelRatio);
        gradient.fixedToCamera = true;
        this.backContainer.addChild(gradient);

        // Flowers
        this.topFlowers = new TopFlowers(this.game);
        this.backContainer.add(this.topFlowers);

        this.bottomFlowers = new BottomFlowers(this.game);
        this.foreContainer.add(this.bottomFlowers);

        // Ground
        var groundBmd = new Phaser.BitmapData(this.game, '1_bg', this.game.width, this.game.gridX(12));
        groundBmd.fill(148,148,64,1);
        var ground = this.backContainer.create(0, this.game.world.height - groundBmd.height, groundBmd);
        ground.fixedToCamera = true;
        ground.setScaleMinMax(2,2,4,4);
        ground.scale.setTo(2*window.devicePixelRatio, 2*window.devicePixelRatio);

        // A duck
        this.duck = new Duck(this.game);
        this.midContainer.add(this.duck);

        // Rain
        this.rain = new Rain(this.game);
        this.foreContainer.add(this.rain);

        // Scores
        this.scores = new Scores(this.game);
        this.uiContainer.add(this.scores);

        // Defend line
        this.defendLineData = new Phaser.BitmapData(this.game, 'defend_line', this.game.width, this.game.gridX(1));
        this.defendLineData.fill(102,102,37,1);
        this.defendLine = this.uiContainer.create(0, this.game.height - this.game.gridX(1), this.defendLineData);
        this.defendLine.fixedToCamera = true;

        // Clouds
        this.clouds = new Clouds(this.game);
        this.clouds.fixedToCamera = true;
        this.foreContainer.add(this.clouds);

        // Reset positions, visibility, physics, whatever
        this.reset();

        // Inputs
        this.leftArrow = this.game.input.keyboard.addKey(Phaser.KeyCode.LEFT);
        this.rightArrow = this.game.input.keyboard.addKey(Phaser.KeyCode.RIGHT);
        this.spaceKey = this.game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);

        // Flod (.xm player)
        this.flod = window.neoart['F2Player']();
        this.flod.load(this.game.cache.getBinary('level__music'));

        // Finished creating, mark screen init 
        this.isScreenInitialized = true;
        this.onScreenInitialized.dispatch();
    },

    // Screen has been created and transitioned in 
    reset : function() {

        console.log("game screen reset");

        // Remove all buttons
        // this.uiContainer.remove( this.menuButton );
        // this.menuButton.visible = false;

        // Reset
        this.score = 0;
        this.levelOver = false;
        this.levelStarted = false;
        this.defendTimer = this.defendLimit;
        this.spaceKeyNeedsReset = false;

        // World x,y,w,h
        this.game.world.setBounds(0, this.game.gridX(-20), this.levelWidth, this.game.gridX(64+20));


        // Bg strobe
        this.strobe.setStatic('bg2');

        // Ground
        // this.ground.body.immovable = true;
        // this.ground.body.allowGravity = false;

        // Duck
        this.duck.reset();
        this.duck.x = this.game.gridX(30);
        this.duck.y = this.game.gridX(57);

        // Drop
        this.rain.reset();

        // Flowers
        this.topFlowers.reset();
        this.bottomFlowers.reset();

        // Sounds
        this.jsfxCounter = 0;
        this.collectSoundCounter = 0;

        // Shake
        this.shakeTimer = null;

        // Defend Line
        this.drawLine();

        // Clouds
        this.clouds.reset();

    },

    playScreen : function() {
        
        console.log("game screen play");

        // Add listeners
        // this.menuButton.events.onInputUp.add();

        if ( ! this.levelStarted ) {
            // Start... or move to wait for an input or something
            this.start();

            return;
        }

        // Music (resume)
        this.flod.play();

    },

    pauseScreen : function() {

        console.log("game screen pause");

        // Remove listeners
        // this.menuButton.events.onInputUp.removeAll();

        // Rain
        this.rain.pause();

        // Scores
        this.scores.pause();

        // Clouds
        this.clouds.pause();

        // Music (pause)
        this.flod.pause();
    },

    // After the user "flaps" or some other input
    start : function() {

        console.log("level away!");

        // Bug?
        this.gameContainer.x = 0;

        // Start update process
        this.levelStarted = true;

        // TODO: Last minute fix so world can move, fix maybe
        this.gameContainer.fixedToCamera = false;

        // Duck
        this.duck.start();
        this.duck.onDefendStart.add(this.onDefendStartHandler.bind(this));

        // Rain, 10s?
        setTimeout(function() {
            this.rain.start();
        }.bind(this), 10000)

        // Scores
        this.scores.start();

        // Flowers
        this.topFlowers.start();
        this.topFlowers.addFlower(this.game.gridX(70));

        this.bottomFlowers.start();
        this.bottomFlowers.addFlower(this.game.gridX(10));
        this.bottomFlowers.addFlower(this.game.gridX(30));

        // Clouds
        this.clouds.start();

        // Music (initial play)
        this.flod.play();

    },

    updateBeforeTransitionIn : function() {

    },

    onDefendStartHandler : function() {

        // Only allowed to defend for so long
        this.defendTimer = this.defendLimit;

    },

    levelWinHandler : function() {

        // Write current score
        this.game.currentScore = this.score;

    },

    levelLoseHandler : function() {

        console.log("game over :(");
        console.log("score: "+ this.score);

        this.levelOver = true;

        // Write current score
        this.game.currentScore = this.score;

    },

    update : function() {
        //console.log("game screen update");

        //console.log(this.game.time.time +", "+ this.game.time.now +", "+ this.taco.y);

        if( this.levelStarted ) {

            if( this.levelOver ) {
                // Currently dead. Viewing their score, waiting to reset.

                console.log("dead.");

            }
            else {
                // Playing...

                // console.log("playing...");

                // Lock running an update to every fourth frame. (~15fps)
                if( this.frameCounter % 4 === 0 ) {
                    
                    // Cursors
                    if( this.leftArrow.isDown ) {
                        // Move left
                        this.duck.walkLeft();
                        this.duck.x -= this.game.gridX(1);
                    }
                    else if( this.rightArrow.isDown ) {
                        // Move right
                        this.duck.walkRight();
                        this.duck.x += this.game.gridX(1);
                    }

                    // Space
                    if( this.spaceKey.isDown ) {

                        this.drawLine();

                        if( ! this.spaceKeyNeedsReset ) {
                            // Allow umbrella
                            this.spaceKeyNeedsReset = true;
                            this.duck.defend();
                        }
                    }
                    else {
                        // Can't umbrella again until space is lifted
                        this.spaceKeyNeedsReset = false;
                        this.defendTimer = this.defendLimit;
                        this.drawLine();
                        this.duck.relax();
                    }

                    // Only allow defend for so long
                    if( this.duck.isDefending && this.defendTimer > 0 ) {
                        this.defendTimer--;

                        this.drawLine();

                        // Okay, thats enough
                        if( this.defendTimer === 0 ) {
                            this.duck.relax();
                        }
                    }

                    // Update camera
                    var newCameraX = this.duck.x - (this.game.width * 0.5);
                    // Only move forward
                    if( newCameraX > this.game.camera.x ) {
                        this.game.camera.x = newCameraX;
                        
                        // Parallax bottom flowers
                        this.bottomFlowers.x = -newCameraX*0.5;
                    }

                    // Bounds
                    if( this.duck.x < this.game.camera.x ) {
                        this.duck.x = this.game.camera.x;
                    }
                    else if( this.duck.x > this.game.camera.x + this.game.width ) {
                        this.duck.x = this.game.camera.x + this.game.width;
                    }

                }

                // Collision on raindrops
                this.rain.iterate('alive', true, Phaser.Group.RETURN_TOTAL, function(activeDrop) {

                    // Not sure why we have to manually update each drop...
                    activeDrop.update();

                    // Collision check
                    if( activeDrop.falling && ! activeDrop.animating ) {

                        if( this.game.checkOverlap(activeDrop, this.duck) ) {

                            // Explode animation
                            activeDrop.explode();

                            if( this.duck.isDefending ) {
                                // Get points
                                this.scores.addScore(this.duck.x - this.game.gridX(8) + this.game.gridX(Math.floor(Math.random()*16)), this.duck.y - this.game.gridX(20) - this.game.gridX(Math.floor(Math.random()*8)), '10');

                                if( this.collectSoundCounter <= 0 ) {

                                    // Wait to play this annoying sound again
                                    this.collectSoundCounter = 2;

                                    // Play sound from one of the 3 channels
                                    if( this.jsfxCounter % 4 == 0 ) {
                                        this.game.jsfxSamples.collect.play();
                                    }
                                    else if( this.jsfxCounter % 4 == 1 ) {
                                        this.game.jsfxSamples2.collect.play();
                                    }
                                    else if( this.jsfxCounter % 4 == 2 ) {
                                        this.game.jsfxSamples3.collect.play();
                                    }
                                    else {
                                        this.game.jsfxSamples4.collect.play();
                                    }

                                    this.jsfxCounter += Math.floor(Math.random() * 3);
                                }
                            }
                            else {
                                // Hit duck

                                // Shake camera
                                this.game.shakeCounter = 15;
                                this.shakeTimer = setInterval(this.game.shakeCamera.bind(this), 20);

                                // Play sound from one of the 3 channels
                                if( this.jsfxCounter % 4 == 0 ) {
                                    this.game.jsfxSamples.hurt.play();
                                }
                                else if( this.jsfxCounter % 4 == 1 ) {
                                    this.game.jsfxSamples2.hurt.play();
                                }
                                else if( this.jsfxCounter % 4 == 2 ) {
                                    this.game.jsfxSamples3.hurt.play();
                                }
                                else {
                                    this.game.jsfxSamples4.hurt.play();
                                }

                                this.jsfxCounter += Math.floor(Math.random() * 3);

                            }
                        }
                    }

                }.bind(this));

                // Decrement wait time for jsfx sound
                if( this.collectSoundCounter > 0 )
                    this.collectSoundCounter--;
            }

            // Update clouds


        }
        else {
            // Game is reset, waiting for user to begin play
        }
        
        // Mark when screen updates
        // this.updateCounter++;

        // Mark every actual frame
        this.frameCounter++;
    },

    drawLine : function() {

        this.defendLineData.clear();

        this.defendLineData.width = Math.floor(this.game.gridX(64) * this.defendTimer/this.defendLimit);
        this.defendLineData.fill(102,102,37,1);
    },

    render : function() {
        
        // this.game.debug.bodyInfo(this.duck.getSprite(), 32, 32);
        // this.game.debug.body(this.duck.getSprite());
        // this.game.debug.body(this.ground);

    },

    destroy : function() {

    }

};
