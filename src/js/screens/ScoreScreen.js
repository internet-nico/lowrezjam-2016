
BasicGame.ScoreScreen = function() {

    // Public signals
    this.onScreenInitialized = new Phaser.Signal;
    this.scoreReadyToTransition = new Phaser.Signal;

    // Screen
    this.game;
    this.id = "score";
    this.container;
    this.isScreenInitialized = false;
    this.updateBeforeTransitionIn;

    this.hiScoreText;
    this.scoreWaiting = false;

};

BasicGame.ScoreScreen.prototype = {

    create : function( game, gameContainer ) {

        console.log("create "+ this.id +" screen");

        // Hold a copy of the game
        this.game = game;

        // Add this screens container as child of the main container
        this.container = new Phaser.Group(this.game, gameContainer);

        var bmd = new Phaser.BitmapData(this.game, 'bg1', this.game.width, this.game.height);
        bmd.fill(102,102,37,1);
        var bg = new Phaser.Sprite(this.game, 0, 0, bmd);

        this.container.add(bg);

        // Hi-Score information
        // var hiScoreText = new Phaser.BitmapText(
        //     this.game, this.game.width - (48*window.devicePixelRatio), this.game.gridX(20),
        //     'visitor_160_light', 'Hi-Score\n'+ ("00000000" + game.hiScore.toString()).substr(-8,8), 80*window.devicePixelRatio, 'right'
        // );
        // hiScoreText.anchor.setTo(1, 0);

        // this.container.add(hiScoreText);

        // Current Score
        var scoreText = new Phaser.BitmapText(
            this.game, this.game.width - (48*window.devicePixelRatio), this.game.gridX(10),
            'visitor_160_light', 'Score\n'+ ("00000000" + game.currentScore.toString()).substr(-8,8), 80*window.devicePixelRatio, 'right'
        );
        scoreText.anchor.setTo(1, 0);

        this.container.add(scoreText);

        // Current Level
        var levelText = new Phaser.BitmapText(
            this.game, this.game.width * 0.5, this.game.gridX(40),
            'visitor_160_light', 'Stage '+ ("00" + game.currentLevel).substr(-2,2), 80*window.devicePixelRatio, 'center'
        );
        levelText.anchor.setTo(0.5, 0);

        this.container.add(levelText);

        // Reset positions and visibility
        this.reset();

        // Finished creating, mark screen init 
        this.isScreenInitialized = true;
        this.onScreenInitialized.dispatch();
    },

    // Screen has been created and transitioned in 
    playScreen : function() {

        if( this.scoreWaiting ) 
            return;

        this.scoreWaiting = true;
        
        console.log("score screen play");

        // Wait,  
        setTimeout(function() {

            this.scoreWaiting = false;

            // then signal to Game that we're ready to transition to the current level
            this.scoreReadyToTransition.dispatch();

        }.bind(this), 1600);

    },

    pauseScreen : function() {

        console.log("score screen pause");

    },

    reset : function() {

        console.log("game screen reset");

        // Flash a gameboy-ish white screen
        // this.container.add( this.whiteRect );
        // this.whiteRect.visible = true;
        // this.whiteRect.alpha = 1.0;

        // setTimeout(function() {
        //     this.whiteRect.alpha = 0.75;

        //     setTimeout(function() {
        //         this.whiteRect.alpha = 0.5;

        //         setTimeout(function() {
        //             this.whiteRect.alpha = 0.25;

        //             setTimeout(function() {
        //                 this.whiteRect.alpha = 0;
        //                 this.whiteRect.visible = false;
        //                 this.container.remove( this.whiteRect );


        //             }.bind(this), 250);
        //         }.bind(this), 250);
        //     }.bind(this), 250);
        // }.bind(this), 250);

    },

    updateBeforeTransitionIn : function() {

    },

    update : function() {

    },

    render : function() {

    },

    destroy : function() {

    }

};
