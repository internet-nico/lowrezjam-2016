
// Thanks greensock

// For type of tween, input progress ratio (0 - 1) and receive a modified tween ratio
// type: 1 is easeOut, 2 is easeIn, 3 is easeInOut
// p: progress 0-1
BasicGame.EaseHelper = function() {

    return {

        p2EaseRatio : function(type, p) {

            switch( type ) {
                case 1 : 
                    var r = 1 - p;
                    
                    // Pow2 ease
                    r *= r * r;

                    return 1 - r;
                break;
                case 2 : 
                    r = p;

                    // Pow2 ease
                    r *= r * r;

                    return r;
                break;
                case 3 : 
                    r = (p < 0.5) ? p * 2 : (1 - p) * 2;
                    
                    // Pow2 ease
                    r *= r * r;

                    return (p < 0.5) ? r / 2 : 1 - (r / 2);
                break;
            }
        },

        p4EaseRatio : function(type, p) {

            switch( type ) {
                case 1 : 
                    var r = 1 - p;
                    
                    // Pow2 ease
                    r *= r * r * r * r;

                    return 1 - r;
                break;
                case 2 : 
                    r = p;

                    // Pow2 ease
                    r *= r * r * r * r;

                    return r;
                break;
                case 3 : 
                    r = (p < 0.5) ? p * 2 : (1 - p) * 2;
                    
                    // Pow2 ease
                    r *= r * r * r * r;

                    return (p < 0.5) ? r / 2 : 1 - (r / 2);
                break;
            }
        },

        bounceOutRatio : function(p) { 

            if (p < 1 / 2.75) {
                return 7.5625 * p * p;
            } else if (p < 2 / 2.75) {
                return 7.5625 * (p -= 1.5 / 2.75) * p + .75;
            } else if (p < 2.5 / 2.75) {
                return 7.5625 * (p -= 2.25 / 2.75) * p + .9375;
            } else {
                return 7.5625 * (p -= 2.625 / 2.75) * p + .984375;
            }
            
        }
    }

};
