
BasicGame.Preloader = function(game) {

	this.ready = false;

};

BasicGame.Preloader.prototype = {

	preload: function() {

        // Atlas
        this.load.atlas('duck_quest', 'assets/duck_quest.png', 'assets/duck_quest.json');
        
        // Singles
        this.load.image('pause_arrow_big', 'assets/pause_arrow_big.png');
        
        // Title page
        this.load.image('title__duck', 'assets/title__duck.png');
        this.load.image('title__gradient', 'assets/title__gradient.png');
        this.load.image('title__duck-quest', 'assets/title__duck-quest.png');

        // Level
        this.load.image('level__gradient', 'assets/level__gradient.png');
        this.load.binary('level__music', 'assets/duck_quest-3.xm');

        // Fonts
        this.game.load.bitmapFont('gameboy_128_light', 'assets/fonts/gameboy_128_light.png', 'assets/fonts/gameboy_128_light.xml');
        this.game.load.bitmapFont('gameboy_128_dark', 'assets/fonts/gameboy_128_dark.png', 'assets/fonts/gameboy_128_dark.xml');
        this.game.load.bitmapFont('visitor_160_light', 'assets/fonts/visitor_160_light.png', 'assets/fonts/visitor_160_light.xml');
        this.game.load.bitmapFont('visitor_160_dark', 'assets/fonts/visitor_160_dark.png', 'assets/fonts/visitor_160_dark.xml');
        this.game.load.bitmapFont('tom_128_light', 'assets/fonts/tom_thumb_light.png', 'assets/fonts/tom_thumb_light.xml');

	},

	create: function() {

		//	Once the load has finished we disable the crop because we're going to sit in the update loop for a short while as the music decodes
		// this.preloadBar.cropEnabled = false;

        this.ready = true;
        this.state.start('Game');

	}
    // ,
    // If sounds need to decode...
    // update: function() {
		
        //this.cache.isSoundDecoded('titleMusic') && 
		// if(this.ready == false)
		// {
		// this.ready = true;
		// this.state.start('Game');
		// }

	// }

};
