/*
* 
* This is the main and ~only~ game state active the project
* It acts as it's own state manager to control which sub-view, or "screen", is 
* currently working to handle the core "update" and "render" events. 
* 
* All screens are pre-allocated at the start by the ScreenManager
* 
* Screens are transitioned visually by the LinearSlideTransition or some other transition
* 
* When transition starts, both screens will "pause", and eventually the 
* current active screen will "play" and receive events to update and render
* 
* This state also handles a global "pause" menu as well as any level routing
*
* Color Palette:
* 
* #666625 - dark
* #949440
* #d0d066
* #ffff94 - light
*
*/
BasicGame.Game = function(game) {

    // When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    this.game;      // a reference to the currently running game (Phaser.Game)
    this.add;       // used to add sprites, text, groups, etc (Phaser.GameObjectFactory)
    this.camera;    // a reference to the game camera (Phaser.Camera)
    this.cache;     // the game cache (Phaser.Cache)
    this.input;     // the global input manager. You can access this.input.keyboard, this.input.mouse, as well from it. (Phaser.Input)
    this.load;      // for preloading assets (Phaser.Loader)
    this.math;      // lots of useful common math operations (Phaser.Math)
    this.sound;     // the sound manager - add a sound, play one, set-up markers, etc (Phaser.SoundManager)
    this.stage;     // the game stage (Phaser.Stage)
    this.time;      // the clock (Phaser.Time)
    this.tweens;    // the tween manager (Phaser.TweenManager)
    this.state;     // the state manager (Phaser.StateManager)
    this.world;     // the game world (Phaser.World)
    this.particles; // the particle manager (Phaser.Particles)
    this.physics;   // the physics manager (Phaser.Physics)
    this.rnd;       // the repeatable random number generator (Phaser.RandomDataGenerator)

    // Screen transitions
    this.container;
    this.screenManager;
    this.currentScreen;
    this.lastScreen;
    this.transition;
    this.initialScreenId = 'title';

    // Keys
    this.backspaceKey;
    this.enterKey;
    this.spaceKey;
    this.upArrowKey;
    this.downArrowKey;

    // Pause menu
    this.gamePaused = false;
    this.currentPauseOption = 'resume'; // 'resume', 'quit'
    this.pauseContainer;
    this.pauseMenuBg;
    this.pauseTitle;
    this.pauseResumeText;
    this.pauseQuitText;
    this.pauseArrow;

    // Other
    this.whiteRect;
    this.jsfxSamples;
    this.altJsfxSamples

};

BasicGame.Game.prototype = {

    // Map pixels
    gridX : function(x) { return x*8*window.devicePixelRatio; },

    // Super basic rect overlap 
    checkOverlap : function(spriteA, spriteB) {
        var boundsA = spriteA.getBounds();
        var boundsB = spriteB.getBounds();

        return Phaser.Rectangle.intersects(boundsA, boundsB);
    },

    create: function() {

        // Add ease helper
        this.ease = new BasicGame.EaseHelper();

        // Global game vars
        this.game.gridX = this.gridX;
        this.game.checkOverlap = this.checkOverlap;
        this.game.currentScore = 0;
        this.game.hiScore = 10000;
        this.game.currentLevel = 1;
        this.game.ease = this.ease;
        this.game.shakeCamera = this.shakeCamera;
        this.game.shakeCounter = 0;

        console.log("create game");

        // Main container
        this.container = this.game.add.group();
        this.container.fixedToCamera = true;

        // Pause container
        this.pauseContainer = this.game.add.group();
        this.pauseContainer.fixedToCamera = true;
        this.pauseContainer.visible = false;

        // ScreenManager
        this.screenManager = new BasicGame.ScreenManager([
            BasicGame.TitleScreen, 
            BasicGame.ScoreScreen, 
            BasicGame.LevelTestScreen,
            BasicGame.Level1Screen
        ]);

        // Transition
        this.transition = new BasicGame.LinearSlideTransition(); //SlideTransition();

        // Listen to signals
        this.screenManager.onScreenChanged.add( this.screenChangedHandler.bind(this) );
        this.transition.onTransitionComplete.add( this.screenTransitionCompleteHandler.bind(this) );

        // Game specific signals
        this.screenManager.getScreen("title").onPlayButtonClicked.add( this.gameStart.bind(this) );
        this.screenManager.getScreen("score").scoreReadyToTransition.add( this.gotoNextLevel.bind(this) );
        // this.screenManager.getScreen("level_1").onWin.add( this.levelWon.bind(this) );
        // this.screenManager.getScreen("level_1").onLose.add( this.levelLost.bind(this) );

        // Add screens container then hide
        var screen;
        for( var key in this.screenManager._screens ) {
            screen = this.screenManager._screens[key];
        }

        // White flash
        var bmd = new Phaser.BitmapData(this.game, 'white_flash', this.game.width, this.game.height);
        bmd.fill(255,255,255,1);

        this.whiteRect = new Phaser.Sprite(this.game, 0, 0, bmd);

        // Pause menu
        var pauseBmd = new Phaser.BitmapData(this.game, 'pause_bg', this.game.width * 0.8, this.game.height * 0.8);
        pauseBmd.fill(255,255,130,1);
        
        this.pauseMenuBg = new Phaser.Sprite(this.game, this.game.width * 0.5, this.game.height * 0.5, pauseBmd);
        this.pauseMenuBg.anchor.setTo(0.5, 0.5);

        // Pause title
        this.pauseTitle = new Phaser.BitmapText(
            this.game, this.game.width*0.5, this.game.gridX(11),
            'gameboy_128_dark', 'Paused', 64*window.devicePixelRatio, 'center'
        );
        this.pauseTitle.anchor.setTo(0.5, 0);

        // Pause resume option
        this.pauseResumeText = new Phaser.BitmapText(
            this.game, this.game.gridX(17), this.game.gridX(28),
            'visitor_160_dark', 'Resume', 80*window.devicePixelRatio, 'left'
        );
        this.pauseResumeText.anchor.setTo(0, 0);

        // Pause quit option
        this.pauseQuitText = new Phaser.BitmapText(
            this.game, this.game.gridX(17), this.game.gridX(38),
            'visitor_160_dark', 'Quit', 80*window.devicePixelRatio, 'left'
        );
        this.pauseQuitText.anchor.setTo(0, 0);

        // Pause option arrow
        this.pauseArrow = new Phaser.Sprite(this.game, this.game.gridX(12), this.game.gridX(30), 'pause_arrow_big');
        this.pauseArrow.setScaleMinMax(1,1,2,2);
        this.pauseArrow.scale.setTo(1*window.devicePixelRatio, 1*window.devicePixelRatio);
        this.pauseArrow.anchor.setTo(0, 0);

        // Keys
        this.backspaceKey = this.game.input.keyboard.addKey(Phaser.KeyCode.BACKSPACE);
        this.upArrowKey = this.game.input.keyboard.addKey(Phaser.KeyCode.UP);
        this.downArrowKey = this.game.input.keyboard.addKey(Phaser.KeyCode.DOWN);
        this.enterKey = this.game.input.keyboard.addKey(Phaser.KeyCode.ENTER);
        this.spaceKey = this.game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);

        // Jsfx
        this.game.jsfxSamples = jsfxlib.createWaves({
            drop : ["square",0.0000,0.0500,0.0000,0.0000,0.0000,0.1640,20.0000,52.0000,190.0000,0.0000,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.1000,0.0000],
            collect : ["square",0.0000,0.0500,0.0000,0.0160,0.0930,0.1980,20.0000,1032.0000,2400.0000,0.0000,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.0000,0.0000],
            explode : ["noise",0.0000,0.06,0.0000,0.4000,0.2880,1.3160,20.0000,421.0000,2400.0000,0.2580,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.0000,0.0000],
            noise : ["noise",0.0000,0.2,0.0590,1.5980,0.5280,1.2380,1403.0000,374.0000,1394.0000,0.1540,0.9980,0.7090,6.0088,0.8375,0.7060,-0.5940,0.2940,0.5000,0.0420,0.1608,0.4540,0.6620,0.5930,-0.2420,0.0310,0.9120,0.7440],
            hurt : ["noise",0.0000,0.0400,0.0000,0.0160,0.0000,0.2060,20.0000,1438.0000,2400.0000,-0.5720,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.0000,0.0000],
        });
        // Repeat flap wave in other jsfx samples so multiple can be played at once
        this.game.jsfxSamples2 = jsfxlib.createWaves({
            drop : ["square",0.0000,0.0500,0.0000,0.0000,0.0000,0.1640,20.0000,51.0000,190.0000,0.0000,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.1000,0.0000],
            collect : ["square",0.0000,0.0500,0.0000,0.0160,0.0930,0.1980,20.0000,1032.0000,2400.0000,0.0000,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.0000,0.0000],
            explode : ["noise",0.0000,0.06,0.0000,0.4000,0.2880,1.3160,20.0000,421.0000,2400.0000,0.2580,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.0000,0.0000],
            noise : ["noise",0.0000,0.2,0.0590,1.5980,0.5280,1.2380,1403.0000,374.0000,1394.0000,0.1540,0.9980,0.7090,6.0088,0.8375,0.7060,-0.5940,0.2940,0.5000,0.0420,0.1608,0.4540,0.6620,0.5930,-0.2420,0.0310,0.9120,0.7440],
            hurt : ["noise",0.0000,0.0400,0.0000,0.0160,0.0000,0.2060,20.0000,1438.0000,2400.0000,-0.5720,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.0000,0.0000],
        });
        // The next two have slightly different sounds to spice it up
        this.game.jsfxSamples3 = jsfxlib.createWaves({
            drop : ["square",0.0000,0.0500,0.0000,0.0000,0.0000,0.1640,20.0000,96.0000,190.0000,0.0000,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.1000,0.0000],
            collect : ["square",0.0000,0.0500,0.0000,0.0160,0.0930,0.1980,20.0000,1032.0000,2400.0000,0.0000,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.0000,0.0000],
            explode : ["noise",0.0000,0.06,0.0000,0.4000,0.2880,1.3160,20.0000,421.0000,2400.0000,0.2580,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.0000,0.0000],
            noise : ["noise",0.0000,0.2,0.0590,1.5980,0.5280,1.2380,1403.0000,374.0000,1394.0000,0.1540,0.9980,0.7090,6.0088,0.8375,0.7060,-0.5940,0.2940,0.5000,0.0420,0.1608,0.4540,0.6620,0.5930,-0.2420,0.0310,0.9120,0.7440],
            hurt : ["noise",0.0000,0.0400,0.0080,0.0340,0.0000,0.1980,20.0000,574.0000,2400.0000,-0.3260,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.2040,0.0000],
        });
        this.game.jsfxSamples4 = jsfxlib.createWaves({
            drop : ["square",0.0000,0.0500,0.0000,0.0000,0.0000,0.1640,20.0000,95.0000,190.0000,0.0000,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.1000,0.0000],
            collect : ["square",0.0000,0.0500,0.0000,0.0160,0.0930,0.1980,20.0000,1032.0000,2400.0000,0.0000,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.0000,0.0000],
            explode : ["noise",0.0000,0.06,0.0000,0.4000,0.2880,1.3160,20.0000,421.0000,2400.0000,0.2580,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.0000,0.0000],
            noise : ["noise",0.0000,0.2,0.0590,1.5980,0.5280,1.2380,1403.0000,374.0000,1394.0000,0.1540,0.9980,0.7090,6.0088,0.8375,0.7060,-0.5940,0.2940,0.5000,0.0420,0.1608,0.4540,0.6620,0.5930,-0.2420,0.0310,0.9120,0.7440],
            hurt : ["noise",0.0000,0.0400,0.0080,0.0340,0.0000,0.1980,20.0000,574.0000,2400.0000,-0.3260,0.0000,0.0000,0.0100,0.0003,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,1.0000,0.0000,0.0000,0.2040,0.0000],
        });

        // Enable inputs
        this.backspaceKey.onDown.add(this.onPauseKeyPressedHandler, this);

        // Start at title screen
        this.screenManager.changeScreen(this.initialScreenId);

    },

    onPauseKeyPressedHandler : function() {

        // If currently playing a level
        if( ! this.gamePaused 
            && this.currentScreen && this.currentScreen.isScreenInitialized 
            && this.currentScreen.id.substr(0,5) === 'level' 
            && ! this.transition.isTransitioning ) {

            // Okay, pause current screen
            this.currentScreen.pauseScreen();
            this.currentScreen.container.alpha = 0.25;
            this.gamePaused = true;

            // Create pause menu
            this.createPauseMenu();
        }

    },

    onResumeAfterPauseHandler : function() {

        if( this.gamePaused ) {
            // Remove pause menu
            this.destroyPauseMenu();

            // Okay, unpause
            this.currentScreen.playScreen();
            this.currentScreen.container.alpha = 1;
            this.gamePaused = false;
        }
    },

    onQuitAfterPauseHandler : function() {

        if( this.gamePaused ) {
            // Reset stuff?
            // this.game.currentLevel = 1;
            // this.game.currentScore = 0;
            // this.game.hiScore = 0;
            // this.currentScreen.reset();

            // Remove pause menu
            this.destroyPauseMenu();

            // Unpause current screen
            this.currentScreen.container.alpha = 1;
            this.gamePaused = false;

            // Reset title screen
            this.screenManager.getScreen('title').reset();
            
            // Go to title screen
            this.screenManager.changeScreen('title');
        }

    },

    createPauseMenu : function() {

        // Enable inputs
        this.upArrowKey.onDown.add(this.upArrowKeyPressedHandler, this);
        this.downArrowKey.onDown.add(this.downArrowKeyPressedHandler, this);
        this.enterKey.onDown.add(this.selectOptionButtonPressedHandler, this);
        this.spaceKey.onDown.add(this.selectOptionButtonPressedHandler, this);

        // Add graphics
        this.pauseContainer.add(this.pauseMenuBg);
        this.pauseContainer.add(this.pauseTitle);
        this.pauseContainer.add(this.pauseResumeText);
        this.pauseContainer.add(this.pauseQuitText);
        this.pauseContainer.add(this.pauseArrow);
        this.pauseContainer.visible = true;

    },

    upArrowKeyPressedHandler : function() {

        // Move pause menu selection up
        if( this.currentPauseOption === 'resume' ) {
            // Already at top
        }
        else {
            this.currentPauseOption = 'resume';
            this.pauseArrow.y = this.game.gridX(30);
        }

    },

    downArrowKeyPressedHandler : function() {

        // Move pause menu selection down
        if( this.currentPauseOption === 'quit' ) {
            // Already at bottom
        }
        else {
            this.currentPauseOption = 'quit';
            this.pauseArrow.y = this.game.gridX(40);
        }

    },

    selectOptionButtonPressedHandler : function() {

        console.log("select option: "+ this.currentPauseOption);

        if( this.currentPauseOption === 'quit' ) {
            // Quit game
            this.onQuitAfterPauseHandler();
        }
        else {
            // Resume game
            this.onResumeAfterPauseHandler();
        }

    },

    destroyPauseMenu : function() {

        // Disable inputs
        this.upArrowKey.onDown.removeAll();
        this.downArrowKey.onDown.removeAll();
        this.enterKey.onDown.removeAll();
        this.spaceKey.onDown.removeAll();

        // Remove graphics
        this.pauseContainer.remove(this.pauseMenuBg);
        this.pauseContainer.remove(this.pauseTitle);
        this.pauseContainer.remove(this.pauseResumeText);
        this.pauseContainer.remove(this.pauseQuitText);
        this.pauseContainer.remove(this.pauseArrow);
        this.pauseContainer.visible = false;

    },

    gameStart : function() {

        // User started game
        this.screenManager.changeScreen("score");

    },

    levelWon : function() {

        console.log("level won!");

        // Increment level
        this.game.currentLevel++;

        // Go to score screen inbetween each level
        this.screenManager.changeScreen("score");

    },

    levelLost : function() {

        console.log("level lost!");

        // Go to score screen inbetween each level
        this.screenManager.changeScreen("score");

    },

    gotoNextLevel : function() {

        // Level logic here

        this.screenManager.changeScreen("level_"+ this.game.currentLevel);

    },

    screenChangedHandler: function(lastScreen, currentScreen) {

        if( ! currentScreen ) 
            return;

        // Pause screen
        if( lastScreen ) 
            lastScreen.pauseScreen();

        // Set last/current
        this.lastScreen = lastScreen;
        this.currentScreen = currentScreen;

        if( ! lastScreen ) 
            console.log("Starting on screen "+ currentScreen.id);
        else 
            console.log("Changing screen from "+ lastScreen.id +" to "+ currentScreen.id);

        // If the screen has not been created
        if( ! this.currentScreen.isScreenInitialized ) {

            this.currentScreen.onScreenInitialized.addOnce( this.screenInitializedHandler.bind(this) );

            this.currentScreen.create( this.game, this.container );

        }
        else {

            // Then transition
            this.transitionStart();

        }

    },

    screenInitializedHandler : function() {

        // If this is the title screen...
        if( this.currentScreen.id === this.initialScreenId ) {
            // Just display the very first screen
            this.skipTransition();
        }
        else {
            this.transitionStart();
        }

    },

    skipTransition : function() {

        // Move the new screen to top
        this.container.bringToTop(this.currentScreen.container);
        this.currentScreen.container.x = 0;
        this.currentScreen.container.y = 0;
        this.currentScreen.container.visible = true;

        console.log("skipping transition!");

        // Play immediately
        this.currentScreen.playScreen();
    },

    transitionStart : function() {

        // Allow pages to reflow before transition in
        if( typeof this.currentScreen.updateBeforeTransitionIn === 'function' ) {
            this.currentScreen.updateBeforeTransitionIn();
        }

        // TODO: Last minute fix, useful during transition, not during game
        this.container.fixedToCamera = true;
        
        // Move the new screen to top
        this.container.bringToTop(this.currentScreen.container);

        // Default transition direction, will be random
        var fromDirection = null;

        // Initial start screen
        if( ! this.lastScreen ) 
            fromDirection = 'right';
        else if( this.currentScreen.id === 'score' )
            fromDirection = 'right';
        else if( this.currentScreen.id === 'title' ) 
            fromDirection = 'left';
        else if( this.currentScreen.id.substr(0,5) === 'level' )
            fromDirection = 'right';

        // Run our transition
        this.transition.transition(
            this.game, 
            ((this.lastScreen) ? this.lastScreen : null), 
            this.currentScreen, 
            this.container, 
            fromDirection
        );

    },

    screenTransitionCompleteHandler : function() {

        console.log("transition complete");

        this.currentScreen.playScreen();

    },

    shakeCamera : function() {
        if( this.game.shakeCounter > 0 ) {
            var min = -2;
            var max = 2;
            //this.container.x += Math.floor(Math.random() * (max - min + 1)) + min;
            //this.container.y += Math.floor(Math.random() * (max - min + 1)) + min;
            this.container.x = Math.floor(Math.sin(4 * (Math.floor(Math.random() * (max - min + 1)) + min))*(8*window.devicePixelRatio));
            this.container.y = Math.floor(Math.sin(4 * (Math.floor(Math.random() * (max - min + 1)) + min))*(8*window.devicePixelRatio));

            this.game.shakeCounter--;
        }
        else {
            this.container.x = 0;
            this.container.y = 0;

            clearInterval(this.shakeTimer);
        }
    },

    update: function() {

        // Offset the main loop to the current screen
        if( ! this.gamePaused && this.currentScreen && this.currentScreen.isScreenInitialized && ! this.transition.isTransitioning ) {

            this.currentScreen.update();

        }
        
    },

    render: function() {

        // Offset the main loop to the current screen
        if( ! this.gamePaused && this.currentScreen && this.currentScreen.isScreenInitialized && ! this.transition.isTransitioning ) {

            this.currentScreen.render();

        }

    },

    quitGame: function(pointer) {

        //  Here you should destroy anything you no longer need.
        //  Stop music, delete sprites, purge caches, free resources, all that good stuff.

        console.log("game quit");

    }

};
