var BasicGame = {};

BasicGame.Boot = function (game) {

};

BasicGame.Boot.prototype = {

    init: function () {

        // Initial background
        this.stage.backgroundColor = '#ffffff'; //'#666625';

        // Unless you specifically know your game needs to support multi-touch I would recommend setting this to 1
        this.input.maxPointers = 1;

        // Take into account retina and other pixel-dense devices
        this.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;

        this.scale.setUserScale(1/window.devicePixelRatio, 1/window.devicePixelRatio);

        this.game.stage.scale.minWidth = 512;
        this.game.stage.scale.minHeight = 512;
        this.game.stage.scale.maxWidth = 512;
        this.game.stage.scale.maxHeight = 512;
        this.game.stage.scale.forceLandscape = true;
        this.game.stage.scale.pageAlignHorizontally = true;

        // if (this.game.device.desktop)
        // {
        //     //  If you have any desktop specific settings, they can go in here
        //     this.scale.pageAlignHorizontally = false;
        // }
        // else
        // {
        //  //  Same goes for mobile settings.
        //  //  In this case we're saying "scale the game, no lower than 480x260 and no higher than 1024x768"
        //  
        //  // this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        // }

    },

    preload: function () {

        //  Here we load the assets required for our preloader (in this case a background and a loading bar)
        // this.load.image('preloaderBackground', 'assets/preloader_background.jpg');
        // this.load.image('preloaderBar', 'assets/preloader_bar.png');
        
        //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
        this.stage.disableVisibilityChange = true;

    },

    create: function () {

        //  By this point the preloader assets have loaded to the cache, we've set the game settings
        //  So now let's start the real preloader going
        this.state.start('Preloader');

    }

};
