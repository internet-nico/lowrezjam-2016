
BasicGame.ScreenManager = function ( screensToAllocate ) {

    // Public signal
    this.onScreenChanged = new Phaser.Signal;

    // Private properties
    this._screens = new Array();
    this._lastScreen;
    this._currentScreen;

    // Preallocate screens
    var screen;
    for( var index in screensToAllocate ) {

        // Check if valid
        if( ! index || typeof screensToAllocate[index] === 'undefined' )
            continue;

        // Create instance of screen
        screen = new screensToAllocate[index]();

        console.log("preallocating screen "+ screen.id );

        // Add to array
        this._screens[screen.id] = screen;
    }

    console.log(" + ScreenManager init");

};

BasicGame.ScreenManager.prototype = {

    getScreen : function(id) {

        return this._screens[id];

    },

    changeScreen : function(id) {

        this._lastScreen = this._currentScreen;
        this._currentScreen = this.getScreen(id);


        if( ! this._currentScreen ) {
            this._currentScreen = this._lastScreen;
            return;
        }
        else {
            if( this._currentScreen.isScreenInitialized ) {
                // Reset
                this._currentScreen.reset();
            }
        }

        this.onScreenChanged.dispatch( this._lastScreen, this._currentScreen );

    }

};
