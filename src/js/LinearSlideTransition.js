
BasicGame.LinearSlideTransition = function() {

    // Public signals
    this.onTransitionComplete = new Phaser.Signal;

    this.isTransitioning = false;
    this.inTimerFinished = false;
    this.outTimerFinished = false;

    this.stepDistance = 16; //px
    this.stepInterval = 66; //ms (~15fps)

    this.mask1;
    this.mask2;

    // References
    // this.currentScreen;
    // this.lastScreen;

};

BasicGame.LinearSlideTransition.prototype = {

    transition : function( game, lastScreen, currentScreen, container, direction ) {

        // this.currentScreen = currentScreen;
        // this.lastScreen = lastScreen;

        // Create masks
        if( ! this.mask1 ) {
            this.mask1 = new Phaser.Graphics(game, 0, 0);
            // this.mask1.beginFill(0xffffff);
            // this.mask1.drawRect(currentScreen.container.x, 0, game.width, game.height);
        }

        if( ! this.mask2 ) {
            this.mask2 = new Phaser.Graphics(game, 0, 0);
        }

        direction = direction || (function() {
            // If no direction was supplied...
            var rng = Math.random();

            // Pick a random direction to slide to
            if( rng > 0.5 ) return "left";
            else return "right";
        })();

        // Reset 
        this.isTransitioning = true;
        this.inTimerFinished = false;
        this.outTimerFinished = false;

        // Remove old page
        if( lastScreen ) {

            var targetX = 0;
            var targetY = 0;

            if( direction === "left" )
                targetX = game.width;
            else 
                targetX = -game.width;

            // Find the num 16px steps required to travel the distance
            var outSteps = Math.floor(Math.abs(targetX/(this.stepDistance*window.devicePixelRatio)));
                
            // Create an interval
            var outTimer = new Phaser.Timer(game, true);

            // Every 50ms, for num outSteps, loop
            var i = 0;
            outTimer.repeat(this.stepInterval, outSteps+1, function() {

                // If final loop
                if( i === outSteps ) {
                    // Set to destination value
                    lastScreen.container.x = targetX;
                }
                else {
                    // Keep animating
                    if( direction === "left" )
                        lastScreen.container.x += this.stepDistance * window.devicePixelRatio;
                    else
                        lastScreen.container.x -= this.stepDistance * window.devicePixelRatio;
                }

                i++;
            }, this);

            // On complete, finish transition
            outTimer.onComplete.add(function() {
                
                console.log(" - Screen: " + lastScreen.id );

                // Need to remove page?
                lastScreen.container.visible = false;
                // container.remove(lastScreen.container);

                // Alert class that a single timer has finished
                this.outTimerFinished = true;
                this.timerFinished(lastScreen);

            }, this);

            // Add to pool
            game.time.add(outTimer);

            // Start timer
            outTimer.start();

        }

        // Position new page
        currentScreen.container.visible = true;
        currentScreen.container.x = 0;
        currentScreen.container.y = 0;
        
        if( direction === "left" )
            currentScreen.container.x = -game.width;
        else
            currentScreen.container.x = game.width;

        // Apply mask
        currentScreen.container.mask = this.mask1;

        // Position mask
        this.positionMask(game, currentScreen, this.mask1);

        // Need to add page?
        // container.add(currentScreen.container);

        // Find the num of steps required to travel the distance
        var inSteps = Math.floor(Math.abs(currentScreen.container.x/(this.stepDistance*window.devicePixelRatio)));
            
        // Create an interval
        var inTimer = new Phaser.Timer(game, true);

        // Every 50ms, for num inSteps, loop
        var j = 0;
        inTimer.repeat(this.stepInterval, inSteps+1, function() {

            // If final loop
            if( j === inSteps ) {
                // Set to destination value
                currentScreen.container.x = 0;
            }
            else {
                // Keep animating
                if( direction === "left" )
                    currentScreen.container.x += this.stepDistance * window.devicePixelRatio;
                else
                    currentScreen.container.x -= this.stepDistance * window.devicePixelRatio;

                // Position mask
                this.positionMask(game, currentScreen, this.mask1);
            }

            j++;
        }, this);

        // On complete, finish transition
        inTimer.onComplete.add(function() {

            console.log(" + Screen: " + currentScreen.id );

            // Remove mask
            currentScreen.container.mask = null;

            // Alert class that a single timer has finished
            this.inTimerFinished = true;
            this.timerFinished(lastScreen);
        }, this);

        // Add to pool
        game.time.add(inTimer);

        // Start timer
        inTimer.start();

    },

    positionMask : function(game, screen, mask) {

        mask.clear();
        mask.beginFill(0xffffff);
        mask.drawRect(screen.container.x, 0, game.width, game.height);

    },

    timerFinished : function(lastScreen) {

        if( this.inTimerFinished && ( ! lastScreen || this.outTimerFinished ) ) {

            // Finally, after ALL animations complete...
            this.isTransitioning = false;
            this.onTransitionComplete.dispatch();
        }
    }
};
