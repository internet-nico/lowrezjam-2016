I made this template to be used especially with Flixel[1] and FlodXM[2] to get that ultimate 8bit retro feel with some Flash Game projects, and thought it'd be cool to share!

All samples are ripped from a WAV export from Famitracker[3], including some samples from Bucky's DPCM Rips[4]. I downsampled them a bit and trimmed them to use as little space as possible, so we can keep those load times down on our games!

I also included .xi instruments so that you can add them quickly (without having to export the instrument and load it back up) in case you don't feel like doing the envelopes in the sequencer and need more than one pulse instrument (for example).

You can check out the example file for how I like to use this (Two Pulse Channels, Triangle, and Noise/DPCM channels merged), but you can go nuts and do some fake-bit sequencing.

It would be cool to have more samples, but the tiny drum samples included ate up so much space on their own I thought I'd keep it as simple as possible - two kick drums, two snares, and open and closed hats[5]. I would have added some noise samples, but they need to be pretty long in order for your ears not to pick up a pattern in the repeating sound, so no noise :/

Anyway, I hope you have fun! Share your creations :)

-B.Ultra&Basic/01010111



[1] http://flixel.org/

[2] https://github.com/photonstorm/FlodXM

[3] http://famitracker.shoodot.net/

[4] http://famitracker.shoodot.net/forum/posts.php?id=294

[5] Here's the mapping for the drum samples:

 [C#][D#]
[C][D][E][F]

= (When entering notes in the tracker via QWERTY:)

 [S][D]
[Z][X][C][V]

C/Z - Kick Drum #1

C#/S - Kick Drum #2

D/X - Snare Drum #1

D#/D - Snare Drum #2

E/C - Closed Hihat

F/V - Open Hihat