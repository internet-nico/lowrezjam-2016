# Duck Quest

For [#lowrezjam 2016](https://itch.io/jam/lowrezjam2016/)

Made with [PhaserJS](http://phaser.io/)

Available to play online at itch.io, [here](https://nicotr014.itch.io/duck-quest)

## Requirements
`node`, `npm`

## Installation

```bash
cp .env.sample .env
npm install 
npm start
```
These steps will create an environment file, install `node` packages via `npm`, and start the expressjs server.
Set your desired port inside the .env file then access `http://localhost:PORT` to view.

### Notes for windows os
```cmd
taskkill /f /im node.exe
```